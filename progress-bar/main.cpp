#include <iostream>
#include <iomanip>

#include <stdlib.h>

#include <unistd.h>

using namespace std;

int main()
{
    /*
    for (int i = 0; i <= 10; ++i)
    {
        cout << "Progress: " << (i*10) << "%" << flush;
        sleep(1);
        cout << "\r" << flush;
    }
    cout << endl;
    */

    system("setterm -cursor off");
    cout << "Progress: " << flush;
    for (int i = 0; i <= 10; ++i)
    {
        cout << setfill(' ') << setw(3) << (i*10) << "%" << flush;
        sleep(1);
        cout << "\b\b\b\b";
    }
    cout << endl;
    system("setterm -cursor on");
    cout << "test" << endl;
    cout << "at" << endl;

    return 0;
}
