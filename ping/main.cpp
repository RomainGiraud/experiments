#include <iostream>
#include <string>
#include <exception>

#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>

#include <stdlib.h>

using namespace std;

namespace
{
    uint16_t cksum(uint16_t *addr, int len)
    {
        int count = len;
        register uint32_t sum = 0;
        uint16_t answer = 0;

        // Sum up 2-byte values until none or only one byte left.
        while (count > 1) {
            sum += *(addr++);
            count -= 2;
        }

        // Add left-over byte, if any.
        if (count > 0) {
            sum += *(uint8_t *) addr;
        }

        // Fold 32-bit sum into 16 bits; we lose information by doing this,
        // increasing the chances of a collision.
        // sum = (lower 16 bits) + (upper 16 bits shifted right 16 bits)
        while (sum >> 16) {
            sum = (sum & 0xffff) + (sum >> 16);
        }

        // Checksum is one's compliment of sum.
        answer = ~sum;

        return answer;
    }

    void ping(char *dst_name, int num = 10, double seconds = 1)
    {
        /* Get source IP address */
        char src_name[256];
        if (::gethostname(src_name, sizeof(src_name)) == -1)
        {
            throw runtime_error("gethostname");
        }

        hostent *src_hp;
        if ((src_hp = ::gethostbyname(src_name)) == 0)
        {
            throw runtime_error("gethostbyname");
        }
        in_addr **addrs = (in_addr**)(src_hp->h_addr_list);
        cout << inet_ntoa(*(addrs[0])) << endl;

        char send_buf[84];
        ip *ips = (ip*)send_buf;
        ips->ip_src = (*(in_addr*)src_hp->h_addr);

        /* Get destination IP address */
        hostent *dst_hp;
        if ((dst_hp = ::gethostbyname(dst_name)) == 0)
        {
            throw runtime_error("gethostbyname");
        }
        addrs = (in_addr**)(dst_hp->h_addr_list);
        cout << inet_ntoa(*(addrs[0])) << endl;

        ips->ip_dst = (*(struct in_addr *)dst_hp->h_addr);

        sockaddr_in dst;
        dst.sin_addr = (*(struct in_addr *)dst_hp->h_addr);

        /* Create RAW socket */
        int sock;
        if ((sock = ::socket(AF_INET, SOCK_RAW, IPPROTO_ICMP)) < 0)
        {
            throw runtime_error("socket");
        }

        /* Socket options, tell the kernel we provide the IP structure */
        int on = 1;
        if (::setsockopt(sock, IPPROTO_IP, IP_HDRINCL, &on, sizeof(on)) < 0)
        {
            throw runtime_error("setsockopt");
        }

        /* IP structure, check the ip.h */
        ips->ip_v = 4;
        // IPv4 header length (4 bits): Number of 32-bit words in header = 5
        ips->ip_hl = 5; // IP4_HDRLEN / sizeof (uint32_t)
        ips->ip_tos = 0;
        ips->ip_len = htons(sizeof(send_buf));
        ips->ip_id = htons(321);
        ips->ip_off = htons(0);
        ips->ip_ttl = 255;
        ips->ip_p = IPPROTO_ICMP;
        ips->ip_sum = 0;

        /* ICMP structure, check ip_icmp.h */
        icmp *icmps = (icmp*)(ips + 1);
        icmps->icmp_type = ICMP_ECHO;
        icmps->icmp_code = 0;
        icmps->icmp_id = 123;
        icmps->icmp_seq = 0;

        /* Set up destination address family */
        dst.sin_family = AF_INET;

        /* Loop based on the packet number */
        for (int i = 0; i < num; ++i)
        {
            /* Header checksums */
            ips->ip_sum = 0;
            ips->ip_sum = cksum((uint16_t*)send_buf, ips->ip_hl); // sizeof(ip)
            icmps->icmp_cksum = 0;
            icmps->icmp_cksum = cksum((uint16_t*)icmps, sizeof(send_buf) - sizeof(ip));

            /* Get destination address length */
            int dst_addr_len = sizeof(dst);

            /* Set listening timeout */
            timeval t;
            t.tv_sec = (int)seconds;
            t.tv_usec = (seconds - (int)seconds) * 1000 * 1000;

            /* Set socket listening descriptors */
            fd_set socks;
            FD_ZERO(&socks);
            FD_SET(sock, &socks);

            /* Send packet */
            int bytes_sent = 0;
            if ((bytes_sent = ::sendto(sock, send_buf, sizeof(send_buf), 0, (sockaddr*)&dst, dst_addr_len)) < 0)
            {
                throw runtime_error("sendto");
            }
            else
            {
                cout << "Sent " << bytes_sent << " byte packet..." << endl;

                /* Listen for the response or timeout */
                int result = 0;
                if ((result = ::select(sock + 1, &socks, NULL, NULL, &t)) < 0)
                {
                    throw runtime_error("select");
                }
                else if (result > 0)
                {
                    cout << "Waiting for packet..." << endl;

                    char recv_buf[400];
                    int bytes_recv = 0;
                    if ((bytes_recv = ::recvfrom(sock, recv_buf, sizeof(recv_buf), 0, (sockaddr*)&dst, (socklen_t*)&dst_addr_len)) < 0)
                    {
                        throw runtime_error("recvfrom");
                    }
                    else
                    {
                        cout << "Received " << bytes_recv << " byte packet!" << endl;
                    }
                }
                else // result == 0
                {
                    throw runtime_error("Timeout: Failed to receive packet!");
                }

                icmps->icmp_seq++;
            }
        }
        
        /* close socket */
        close(sock);
    }
}

int main(int argc, char *argv[])
{
    try
    {
        /* Check for valid args */
        if (argc < 2)
        {
            cout << "Usage: " << argv[0] << " <dst_server>\n" << endl;
            throw runtime_error("wrong number of argument");
        }
        char *dest_name = argv[1];

        /* Check for root permissions */
        if (::getuid() != 0)
        {
            throw runtime_error("program must have root privileges");
        }

        int ret0 = system("ping -qc 1 google.fr &> /dev/null");
        int ret1 = system("ping -qc 1 google.fd &> /dev/null");
        cout << "ret0: " << ret0 << endl;
        cout << "ret1: " << ret1 << endl;
        ping(argv[1], 1, 10);
    }
    catch (runtime_error &e)
    {
        cerr << "[error] " << e.what() << endl;
        return 1;
    }
    catch (...)
    {
        cerr << "[error] undefined" << endl;
        return 1;
    }

    return 0;
}
