#include <iostream>
#include <string>

using namespace std;

string operator "" _hr(unsigned long long n)
{
    return string(n, '-');
}

string operator "" _title(unsigned long long n)
{
    return string(n, '#');
}

int main()
{
    cout << 14_hr << endl;
    cout << 3_title << " Hello! " << 3_title << endl;
    cout << 14_hr << endl;

    return 0;
}
