#include <iostream>

using namespace std;

template <class T, T v>
struct my_integral_constant {
    static constexpr T value = v;
    typedef T value_type;
    typedef integral_constant<T,v> type;
    constexpr operator T() { return v; }
};

template <typename T>
struct my_is_void : my_integral_constant<bool, false>
{
};

template <>
struct my_is_void<void> : my_integral_constant<bool, true>
{
};

template<typename T, typename U>
auto add(T t, U u) -> decltype(t)
{
    return t + u;
}

class Test
{
public:
    Test(int _x) : x(_x) {}
    int x;
};

Test& operator,(const Test& a, Test& b)
{
    return b;
}

int main()
{
    cout << std::is_convertible<int, double>::value << endl;
    cout << std::is_convertible<int, string>::value << endl;

    cout << "------------" << endl;

    cout << my_is_void<int>::value << endl;
    cout << my_is_void<double>::value << endl;
    cout << my_is_void<void>::value << endl;

    decltype(my_is_void<int>::value) t = my_is_void<void>::value;
    cout << t << endl;

    cout << add(1, 2.3) << endl;

    Test i = (Test(3), Test(4));
    cout << i.x << endl;

    cout << (5, 6) << endl;

    return 0;
}
