#include <iostream>

using namespace std;

__attribute__((constructor))
static void initialize_navigationBarImages()
{
    fprintf(stdout, "CONS\n");
}

__attribute__((destructor))
static void destroy_navigationBarImages()
{
    fprintf(stdout, "DES\n");
}

class Test
{
public:
    Test()
    {
        cout << "Test::CONS" << endl;
    }

    ~Test()
    {
        cout << "Test::DES" << endl;
    }
};

static Test test;

int main()
{
    cout << "coucou" << endl;

    return 0;
}
