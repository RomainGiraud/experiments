#include <iostream>

using namespace std;

namespace
{

template <typename T_ret, typename T_arg1, typename T_arg2>
T_ret div_all(T_arg1 x, T_arg2 y)
{
    T_ret r = x / y;
    return r;
}

template <typename T>
struct div_trait
{
    typedef T T_div;
};

template <>
struct div_trait<int>
{
    typedef double T_div;
};

template <typename T>
typename div_trait<T>::T_div div(T x, T y)
{
    typedef typename div_trait<T>::T_div ret_t;
    ret_t r = (ret_t)x / (ret_t)y;
    return r;
}

template <typename T>
void fct(T x)
{
    typename T::iter o;
    o = 4;
    x.toto(o);
}

class Test
{
public:
    typedef int iter;
    void toto(iter x) {}
};

}

int main()
{
    fct(Test());

    div(4, 5);
    //cout << div(4, 3) << endl;
    //cout << div(4.0, 3.0) << endl;

    return 0;
}
