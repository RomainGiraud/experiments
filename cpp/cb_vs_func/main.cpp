#include <iostream>
#include <chrono>
#include <functional>

using namespace std;

namespace
{

typedef int (*incr_t)(int);

int incr(int n)
{
    return n + 1;
}

int loop(int m, incr_t f)
{
    int t = 0;
    for (int i = 0; i < m; ++i)
    {
        t += f(i);
    }
    return t;
}

int loop_fct(int m, std::function<int(int)> f)
{
    int t = 0;
    for (int i = 0; i < m; ++i)
    {
        t += f(i);
    }
    return t;
}

}

int main()
{
    int m = 100000;

    {
        auto t1 = std::chrono::high_resolution_clock::now();
        cout << loop(m, incr) << endl;
        auto t2 = std::chrono::high_resolution_clock::now();
        
        std::chrono::duration<double, std::milli> e = t2 - t1;
        cout << " cb: " << e.count() << "ms" << endl;
    }

    {
        auto t1 = std::chrono::high_resolution_clock::now();
        cout << loop_fct(m, [](int n) -> int { return incr(n); }) << endl;
        //cout << loop_fct(m, [](int n){ return n + 1; }) << endl;
        //cout << loop_fct(m, incr) << endl;
        auto t2 = std::chrono::high_resolution_clock::now();
        
        std::chrono::duration<double, std::milli> e = t2 - t1;
        cout << "fct: " << e.count() << "ms" << endl;
    }

    return 0;
}
