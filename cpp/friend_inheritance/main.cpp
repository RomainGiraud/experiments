#include <iostream>

using namespace std;

template <class T>
class Other;

template <int N>
class A
{
public:
    A(int x) : _x(x) {}

protected:
    int _x;
    int n = N;

    friend class Other<A>;
};

class B : public A<4>
{
public:
    B(int x) : A(x) {}

    friend class Other<B>;
};

template <class T>
class Other
{
public:
    Other(T* a) : _a(a) {}
    int value() { return _a->_x; }

private:
    T* _a;
};

int main()
{
    A<4>* a = new B(4);
    Other<B> o((B*)a);
    cout << "value: " << o.value() << endl;

    delete a;

    return 0;
}
