#include <iostream>

using namespace std;

class Test
{
public:
  void a(int a) { cout << "int: " << a << endl; }
  void a(bool a) { cout << "bool: " << a << endl; }
  void a(const string& a) { cout << "string: " << a << endl; }
  //void a(const char* a) { cout << "char*: " << a << endl; }
};

int main()
{
  Test t;
  t.a("coucou");

  return 0;
}
