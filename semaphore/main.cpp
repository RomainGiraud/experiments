#include <iostream>
#include <deque>
#include <iterator>
#include <sstream>
#include <thread>

#include <semaphore.h>

using namespace std;

// https://softwareengineering.stackexchange.com/questions/168367/multithreading-synchronization-interview-question-find-n-words-given-m-threads
int main()
{
    const char *p = R"(
You are given a paragraph , which contain n number of words, you are given m threads. What you need to do is , each thread should print one word and give the control to next thread, this way each thread will keep on printing one word , in case last thread come, it should invoke the first thread. Printing will repeat until all the words are printed in paragraph. Finally all threads should exit gracefully. What kind of synchronization will use?
)";

    std::deque<std::string> container;
    std::istringstream iss(p);
    std::copy(std::istream_iterator<std::string>(iss),
              std::istream_iterator<std::string>(),
              std::back_inserter(container));

    const int nb_thread = 10;
    std::array<thread, nb_thread> threads;
    std::array<sem_t, nb_thread> semaphores;

    for (auto &s : semaphores)
    {
        sem_init(&s, 1, 0);
    }

    for (size_t i = 0; i < nb_thread; ++i)
    {
        threads[i] = std::move(thread([&container, &semaphores, i](){
                    while (true)
                    {
                        sem_wait(&semaphores[i]);
                        if (container.empty())
                        {
                            sem_post(&semaphores[(i + 1) % nb_thread]);
                            break;
                        }
                        //cout << i << ": " << container.front() << endl;
                        cout << container.front() << " ";
                        container.pop_front();
                        sem_post(&semaphores[(i + 1) % nb_thread]);
                    }
                }));
    }
    sem_post(&semaphores[3]);

    for (auto &t : threads)
    {
        t.join();
    }
    cout << '\n';

    for (auto &s : semaphores)
    {
        sem_destroy(&s);
    }


    return 0;
}
